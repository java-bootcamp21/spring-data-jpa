package com.jpa.mapper;

import com.jpa.entity.BaseEntity;

public abstract class BaseMapper<T extends BaseEntity,K> {
    public abstract T dtoToEntity(K dto);
    public abstract K entityToDTO(T entity);
}
