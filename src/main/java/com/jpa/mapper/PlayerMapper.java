package com.jpa.mapper;

import org.springframework.stereotype.Component;

import com.jpa.dto.PlayerDto;
import com.jpa.entity.Player;

@Component
public class PlayerMapper extends BaseMapper<Player, PlayerDto> {

    @Override
    public Player dtoToEntity(PlayerDto dto) {
    	var player = new Player();
    	player.setId(dto.getId());
    	player.setName(dto.getName());
    	player.setNationality(dto.getNationality());
    	player.setBirthDate(dto.getBirthDate());
    	player.setTitles(dto.getTitles());
        return player;
    }

    @Override
    public PlayerDto entityToDTO(Player entity) {
    	var player = new PlayerDto();
    	player.setId(entity.getId());
    	player.setName(entity.getName());
    	player.setNationality(entity.getNationality());
    	player.setBirthDate(entity.getBirthDate());
    	player.setTitles(entity.getTitles());
        return player;
    }
}
