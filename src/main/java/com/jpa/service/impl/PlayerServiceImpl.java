package com.jpa.service.impl;

import com.jpa.dto.PlayerDto;
import com.jpa.mapper.PlayerMapper;
import com.jpa.repository.PlayerRepository;
import com.jpa.service.PlayerService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlayerServiceImpl implements PlayerService {
	
	private final PlayerRepository playerRepository;
	private final PlayerMapper playerMapper;
	
	public PlayerServiceImpl(PlayerRepository playerRepository, PlayerMapper playerMapper) {
		super();
		this.playerRepository = playerRepository;
		this.playerMapper = playerMapper;
	}

	@Override
    public PlayerDto findById(Integer id) {
		var player = playerRepository.findById(id);
        return playerMapper.entityToDTO(player);
    }

    @Override
    public List<PlayerDto> findAll() {
        return playerRepository.findAll().stream()
        		.map(p -> playerMapper.entityToDTO(p))
        		.collect(Collectors.toList());
    }

    @Override
    public PlayerDto createPlayer(PlayerDto player) {
        var p =  playerRepository
        		.createPlayer(playerMapper.dtoToEntity(player));
        return playerMapper.entityToDTO(p);
    }

    @Override
    public PlayerDto updatePlayer(Integer playerId, PlayerDto player) {
        var playerToUpdate = playerMapper.dtoToEntity(findById(playerId));
        playerToUpdate.setName(player.getName());
        playerToUpdate.setNationality(player.getNationality());
        playerToUpdate.setBirthDate(player.getBirthDate());
        playerToUpdate.setTitles(player.getTitles());
    	return playerMapper
    			.entityToDTO(playerRepository
    					.updatePlayer(playerId, playerToUpdate));
    }

    @Override
    public void deletePlayer(Integer playerId) {
    	playerRepository.deletePlayer(playerId);
    }

	@Override
	public List<PlayerDto> findTopPlayers(Integer titles) {
		return playerRepository.findTopPlayers(titles).stream()
				.map(p -> playerMapper.entityToDTO(p))
				.collect(Collectors.toList());
	}

	@Override
	public List<PlayerDto> findPlayersByNationality(String nationality) {
		return playerRepository.findPlayersByNationality(nationality)
				.stream()
				.map(p -> playerMapper.entityToDTO(p))
				.collect(Collectors.toList());
	}
}
