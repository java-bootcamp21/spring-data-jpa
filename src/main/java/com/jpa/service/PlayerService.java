package com.jpa.service;

import com.jpa.dto.PlayerDto;
import com.jpa.entity.Player;

import java.util.List;

public interface PlayerService {

    PlayerDto findById(Integer id);
    List<PlayerDto> findAll();
    PlayerDto createPlayer(PlayerDto player);
    PlayerDto updatePlayer(Integer playerId, PlayerDto player);
    void deletePlayer(Integer playerId);
    List<PlayerDto> findTopPlayers(Integer titles);
    List<PlayerDto> findPlayersByNationality(String nationality);
}
