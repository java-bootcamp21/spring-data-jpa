package com.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

import java.time.LocalDateTime;

@Entity
@NamedQueries({
			@NamedQuery(name = "Player_Find_All",
					query = "select p from Player p"),
			@NamedQuery(name = "Player_Find_Top_Player",
			query = "select p from Player p where p.titles > :titles"),
			@NamedQuery(name = "Player_Find_Player_By_Nationality",
			query = "select p from Player p where p.nationality = :nationality")
		})
public class Player extends BaseEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "name")
	private String name;
	private String nationality;
	private LocalDateTime birthDate;
	private Integer titles;

	public Player() {
		
	}

	public Player(String name, String nationality, LocalDateTime bithDate, Integer titles) {
		this.name = name;
		this.nationality = nationality;
		this.birthDate = bithDate;
		this.titles = titles;
	}
	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public LocalDateTime getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDateTime bithDate) {
		this.birthDate = bithDate;
	}
	public Integer getTitles() {
		return titles;
	}
	public void setTitles(Integer titles) {
		this.titles = titles;
	}
	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", nationality=" + nationality + ", bithDate=" + birthDate
				+ ", titles=" + titles + "]";
	}





}
