package com.jpa.repository;

import com.jpa.entity.Player;

import java.util.List;

public interface PlayerRepository {

    Player findById(Integer id);
    List<Player> findAll();
    Player createPlayer(Player player);
    Player updatePlayer(Integer playerId, Player player);
    void deletePlayer(Integer playerId);
    List<Player> findTopPlayers(Integer titles);
    List<Player> findPlayersByNationality(String nationality);
}
