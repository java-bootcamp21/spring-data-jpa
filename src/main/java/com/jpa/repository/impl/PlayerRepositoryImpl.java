package com.jpa.repository.impl;

import com.jpa.entity.Player;
import com.jpa.repository.PlayerRepository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class PlayerRepositoryImpl implements PlayerRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
    @Override
    public Player findById(Integer id) {
        return entityManager.find(Player.class, id);
    }

    @Override
    public List<Player> findAll() {
    	var typedQuery = entityManager
    			.createNamedQuery("Player_Find_All",Player.class);
        return typedQuery.getResultList();
    }

    @Override
    public Player createPlayer(Player player) {
        return entityManager.merge(player);
    }

    @Override
    public Player updatePlayer(Integer playerId, Player player) {
        player.setId(playerId);
    	return entityManager.merge(player);
    }

    @Override
    public void deletePlayer(Integer playerId) {
    	Player player = entityManager
    			.find(Player.class, playerId);
    	entityManager.remove(player);

    }

	@Override
	public List<Player> findTopPlayers(Integer titles) {
		var typedQuery = entityManager
				.createNamedQuery("Player_Find_Top_Player",Player.class)
				.setParameter("titles", titles);
		return typedQuery.getResultList();
	}

	@Override
	public List<Player> findPlayersByNationality(String nationality) {
        var typedQuery = entityManager
        		.createNamedQuery("Player_Find_Player_By_Nationality",Player.class)
        		.setParameter("nationality", nationality);
		return typedQuery.getResultList();
	}
}
