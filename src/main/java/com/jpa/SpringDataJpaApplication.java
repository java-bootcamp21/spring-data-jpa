package com.jpa;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jpa.dto.PlayerDto;
import com.jpa.service.PlayerService;

@SpringBootApplication
public class SpringDataJpaApplication implements CommandLineRunner {

	private static Logger logger = LoggerFactory
			.getLogger(SpringDataJpaApplication.class);
	
	private final PlayerService playerService;
	
    public SpringDataJpaApplication(PlayerService playerService) {
		super();
		this.playerService = playerService;
	}

	public static void main(String[] args) {
        SpringApplication.run(SpringDataJpaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    	
//    	var playerToCreate = new PlayerDto("Player 5","Albania"
//    			,LocalDateTime.now().minusYears(30),10);
//    	playerToCreate = playerService.createPlayer(playerToCreate);
//    	logger.info("Player Created : {}",playerToCreate);
//    	
//    	var playerToUpdate = playerService.findById(1);
//    	playerToUpdate.setNationality("Kosovo");
//    	playerToUpdate = playerService.updatePlayer(1, playerToUpdate);
//    	logger.info("Update player: {}",playerToUpdate);
//    	playerService.deletePlayer(2);
    	
    	logger.info("player list: {}",playerService.findAll());
    	logger.info("top player list: {}",playerService.findTopPlayers(2));
    	logger.info("player by nationality list: {}",playerService.findPlayersByNationality("Albania"));
    	
    	
    	
    	
    	
    	

    }
}
